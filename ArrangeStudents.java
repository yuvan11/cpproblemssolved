import java.io.*;
import java.util.*;
import java.util.stream.*;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'arrangeStudents' function below.
     *
     * The function is expected to return a STRING.
     * The function accepts following parameters:
     *  1. INTEGER_ARRAY a
     *  2. INTEGER_ARRAY b
     */
    
    public static String arrangeStudents(List<Integer> a, List<Integer> b ) {
    
    
        
        List<Integer> newList = new ArrayList<Integer>();
        
            String yes_no = " " ;
        
        
        for(int i=0 ; i< a.size() ; i++){
          
            
            if(a.get(i) >= b.get(i)){
                
                newList.add(a.get(i));
                newList.add(b.get(i));
                yes_no = "YES";
            }
            else if(b.get(i) >= a.get(i)){
                
                newList.add(b.get(i));
                newList.add(a.get(i));
                
                yes_no = "YES";
                
            }
            
            else 
               yes_no = "NO";
            
            }
                     int N = a.size();
                for(int i=0;i< N; i++){
                    
                    if(newList.get(i) <= newList.get(i+1))
                         yes_no = "YES";
                     
                    
                    else
                         yes_no = "NO";
                } 
                
            
    
            return yes_no;
                    
    }

}

public class ArrangeStudents {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int t = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, t).forEach(tItr -> {
            try {
                int n = Integer.parseInt(bufferedReader.readLine().trim());

                List<Integer> a = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                    .map(Integer::parseInt)
                    .collect(toList());

                List<Integer> b = Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                    .map(Integer::parseInt)
                    .collect(toList());

                String result = Result.arrangeStudents(a, b);

                bufferedWriter.write(result);
                bufferedWriter.newLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        bufferedReader.close();
        bufferedWriter.close();
    }
}
