#include<iostream>

using namespace std ;


// f_A - [l , m ]
// s_A - [m+1 , r]

void merge(int arr[] , int l , int m , int r) {


	int i , j , k , n1 , n2 ;
	
	n1 = m - l + 1 ;
	n2 = r - m ; 


	int L[n1] , R[n2] ;

	for ( i = 0 ; i < n1 ; i++)
		L[i] = arr[l + i] ;
	for(j = 0 ; j < n2 ; j++)
		R[j] = arr [ m+1+j ] ;	


	i = 0 ;
	j = 0 ; 
	k = l ; 
	
	while( i < n1 && j < n2) {


	if(L[i] <= R[i] ){
	
	arr[k] = L[i];
	i++;
	}

	else {

	arr[k] = R[j] ;
	j++;
	}	

	k++ ;

	}

	
	while( i < n1) {
		
	arr[k]  = L[i];
	i++;
	k++;

	}
	
	while( j < n2) {
	
	arr[k]  = R[j];
	j++;
	k++;
	}
	
}

void mergesort(int  arr[] , int l , int r){


	int m ;

	if( l < r ) {

	m = l + (r-l) / 2 ;

	mergesort(arr , l , m);
	mergesort(arr , m+1 , r);
	
	merge( arr , l , m , r);
	
	}

}


int printArray(int arr[] , int size) {

	for(int i =0 ; i < size ; i++ ){

		cout << arr[i] << " " ;
	}
		cout << "\n" ;

}


int main(){

	int arr[] = {12, 11, 13, 5, 6, 7 } ;
	
	int arrsize = sizeof(arr) / sizeof(arr[0]) ;
	

	cout << "Before sorting " << endl ;	
	printArray(arr , arrsize);

	mergesort(arr , 0 , arrsize-1 ) ;

	cout << "After  sorting " << endl ;
	printArray(arr , arrsize);

return 0 ;
}
