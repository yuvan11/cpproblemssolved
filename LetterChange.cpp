/*  Letter Changes
Have the function LetterChanges(str) take the str parameter being passed and modify it using the following algorithm. Replace every letter in the string with the letter following it in the alphabet (ie. c becomes d, z becomes a). Then capitalize every vowel in this new string (a, e, i, o, u) and finally return this modified string.
Examples
Input: "hello*3"
Output: Ifmmp*3
Input: "fun times!"
Output: gvO Ujnft! */


#include<iostream>

	string LetterChanges(string& str){

	/*

	 & is used here  for memory management , helps while getting big inputs .

	 Hence the passed argument str is like 0|1|2|3|4|5|6|7| -- > as character array representation .

	 */

	std::string strnew;
	
	
	for(int i=0 ; i < str[i] != '\0' ; i++ ) 
	
	// As the passed argument is string , it should end with "\0" . so , I need to iterate till "\0" to perform the whole operation

	{ 

		/* case : if  (checking for alphabets of lowercase character only) 
				then increment to next character.

			Let's say hello*3
			|h|e|l|l|0|*|3|
			 0 1 2 3 4 5 6			
			ascii of h -> 104 , so I should print i--> 105 
			
			strnew is the array  which declared to return the changed string finally

				    (strnew + ) -- > used here for concatinating the fetched input from str[i] to strnew 
					
					|| 

			so , strnew = strnew +  (str[i]+1);
				     		('h' --> 104)



		case : else -->  it will append other characters except only lower alpha's



			

		*/


			if(str[i] >= 'a' && str[i] <= 'z') strnew += str[i] + 1;  												
			else strnew += str[i];
		
	}
	
	  int i=0;
/* currently , strnew to be checked till "\0"     if vowels exists ,Replace to CAPS ; else dont replace .
*/


	while(strnew[i] != '\0'){
	    	
		if(strnew[i] == 'a'|| strnew[i] == 'e' || strnew[i]== 'i' || strnew[i] == 'o' || strnew[i] == 'u') 
			strnew[i] = strnew[i] - 32 ; // or strnew += strnew[i] - 32 ; also will work .
	        i++;
	}
	
	return strnew ;
	
}


int main(){


std::string str , strnew;

// getline is to get a string with new spaces , where cin << str doesnot accept new spaces .
std::getline(std::cin, str);

strnew = LetterChanges(str);
std::cout << strnew <<endl;

}
