


Mathematical Formula for implementing Fibonacci Series , The below , two algo's work well up to a n<=30 and n<=70(FOr n >30 and n<70) it will gives exponential result


The mathematical Expression is :



	fib(n) = ( phi^n + (1 - phi)^n ) / sqrt(5)

	where phi = ( 1 + sqrt(5) ) / 2 


This can be simplified bit smaller ,

	fib(n) = ( (phi) ^ n  ) / sqrt(5)

where phi = ( 1 + sqrt(5) ) / 2 


ref2 : https://bosker.wordpress.com/2011/07/27/computing-fibonacci-numbers-using-binet%E2%80%99s-formula/
ref1 : https://stackoverflow.com/questions/9645193/calculating-fibonacci-number-accurately-in-c 
 

Example 1 :

double binet(unsigned int n)
{
    static const double phi = (1 + sqrt(5))*0.5;
    double fib = (pow(phi,n) - pow(1-phi,n))/sqrt(5);
    return round(fib);
}

Example 2 :


double fib_fixed(long n)
{
    double phi = (sqrt(5) + 1) / 2.0;
    return round(pow(phi, n) / sqrt(5));
}





Example 3 : works perfectly till n<=93 , n>=94 bits at the end changes



#include <bits/stdc++.h>

using namespace std;

unsigned long long binet(unsigned int n){
    if (n == 0) return 0;
    unsigned int h = n/2, mask = 1;
    // find highest set bit in n, can be done better
    while(mask <= h) mask <<= 1;
    mask >>= 1;
    unsigned long long a = 1, b = 1, c; // a = F(k), b = F(k+1), k = 1 initially
    while(mask)
    {
        c = a*a+b*b;        // F(2k+1)
        if (n&mask)
        {
            b = b*(b+2*a);  // F(2k+2)
            a = c;          // F(2k+1)
        } else {
            a = a*(2*b-a);  // F(2k)
            b = c;          // F(2k+1)
        }
        mask >>= 1;
    }
    return a;
}

int main() {

 cout << binet(9) << "\n";
 cout << binet(5) << "\n";
 cout << binet(69) << "\n";
 cout << binet(93) << "\n"; 
 cout << binet(94) << "\n";  

//F93	12200160415121876738
//F94	19740274219868223167
	return 0;

}




int main() {
    
    int t , N;
    cin >> t ;
 
   while(t--){ 
       
       cin >> N ;
       calculateFactorial(N);
      
       
       
   }
	return 0;
}



