import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Teamfight {
  public static void main(String[] args) {
    ArrayList<Integer> team1 = new ArrayList<Integer>();
    ArrayList<Integer> team2 = new ArrayList<Integer>();
    int counter = 0;

    Scanner input = new Scanner(System.in);
    int N =input.nextInt();
    for(int i=0 ;i<N ; i++ ){

        team1.add(input.nextInt());
    }
    for(int i=0 ;i<N ; i++ ){

        team2.add(input.nextInt());
    }

    Collections.sort(team1);
    Collections.sort(team2);

    int win_count=0;

		// Opponent vector size is fixed
		for(int i=0;i<N;i++){
			int minimum_number=0;
			int idx=-1;

			for(int j=0;j<team1.size();j++){

				// first check if player can defeat opponent
				if(team1.get(i)>team2.get(i)){

					// check if minimum_number is initialized
					if(minimum_number==0){
						minimum_number=team1.get(i);
						idx=j;
					}
					//then check if we find smaller minimum_number
					else if(team1.get(j)<minimum_number){
						minimum_number=team1.get(j);
						idx=j;
					}
				}

      }
			if(minimum_number!=0){
				//team1.remove(team1.clear()+idx);
				win_count+=1;
			}
		}

System.out.println(win_count);
	
}
}
