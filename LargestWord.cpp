 /* Longest Word
Have the function LongestWord(sen) take the sen parameter being passed and return the largest word in the string. If there are two or more words that are the same length, return the first word from the string with that length. Ignore punctuation and assume sen will not be empty.
Examples
Input: "fun&!! time"
Output: time
Input: "I love dogs"	
Output: love

*/

#include<iostream>
using namespace std;



	string removeSpecialCharacters(string &str1){
		int sizestr1 = str1.size();
			string tempstr1 = "";
	
	
		for(int i=0 ; i < sizestr1 ; i++){

			if ((str1[i] >= 'a' && str1[i] <= 'z') || (str1[i] >= 'A' && str1[i] <= 'Z'))
			 tempstr1 += str1[i];
			
			    

  }
	return tempstr1;
}



	string LetterChanges(string& str){
		
	string tempstring= "" , maxword = "" ;

	for(int i=0 ;  i< str.size() ; i++){
	   
	    
		if(str[i] != ' ')
		{ 	
			tempstring += str[i];
		
		}
		else tempstring = "";

		tempstring = removeSpecialCharacters(tempstring);

		

		if(tempstring.size() > maxword.size()){

			maxword = tempstring;
		
		}


	}
		return maxword;

}


int main(){


std::string str , strnew;


std::getline(std::cin, str);

strnew = LetterChanges(str);

std::cout << strnew <<endl;

}
