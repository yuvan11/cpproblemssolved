		/*  Letter Changes
		Have the function LetterChanges(str) take the str parameter being passed and modify it using the following algorithm. Replace every letter in the string with the letter following it in the alphabet (ie. c becomes d, z becomes a). Then capitalize every vowel in this new string (a, e, i, o, u) and finally return this modified string.
		Examples
		Input: "hello*3"
		Output: Ifmmp*3
		Input: "fun times!"
		Output: gvO Ujnft! */


		import java.io.*;
		import java.util.*;
		class LetterChange{

			static String LetterChanges(String str){
			    
			 // System.out.println(str);

			String strnew = "";
			char[] charstr = str.toCharArray();
			

			
			
			
			for(char ch : charstr){

				if(ch >= 'a' && ch <= 'z') strnew += (char)(ch+ 1);  							
				else strnew += (char)ch;
				
			}
			
	      		//System.out.println(strnew);
			
			  int i=0;
			
 			char[] charstrnew = strnew.toCharArray();
			// System.out.println(charstrnew);
		
			for(char ch : charstrnew){
			    	
				if(ch == 'a'|| ch == 'e' || ch == 'i' || ch == 'o' || ch  == 'u') 
					charstrnew[i] = (char) (ch - 32) ; 
				i++;
				
			}
			
			//System.out.println(charstrnew);
			return String.valueOf(charstrnew);
			
		}


	

		public static void main(String[] args) {


		String str ;

		Scanner scan = new Scanner(System.in);
	       	str= scan.nextLine();
	     	//System.out.println(str);


		System.out.println(LetterChange.LetterChanges(str));
		}
}
		
