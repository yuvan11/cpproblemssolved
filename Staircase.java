//import java.io.*;
//import java.math.*;
//import java.security.*;
//import java.text.*;
import java.util.*;
//import java.util.concurrent.*;
//import java.util.regex.*;

public class Staircase {

    // Complete the staircase function below.
    static void staircase(int n) {
          
          
           
           //solution 1 

           

           for(int i = 1; i <= n; i++) {
            for(int j = 1; j <= n; j++) {
                if(j <= n-i) System.out.print(' ');
                else System.out.print('#');
            }
            System.out.println();
        }


/*
    one line solution in java


int i=0;
    while(++i <= n)
        System.out.println(new String(new char[n-i]).replace("\0", " ")+new String(new char[i]).replace("\0", "#")); 
*/

/*    String str="#";
    for (int i=0;i<n;i++)
        { 
        System.out.printf("%"+n+"s%n",str);
        str=str+"#";
    }
    */
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        staircase(n);

        scanner.close();
    }
}
