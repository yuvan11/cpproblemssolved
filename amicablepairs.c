#include <stdio.h>

int pfacSum(int i)
{
    int sum = 0, p;
    for (p = 1; p <= i / 2; p++)
    {
        if (i % p == 0)
        {
            sum += p;
        }
    }
    return sum;
}

int main()
{
    int a[1000000], i, m, n;
    int start, stop;
    scanf("%d %d", &start, &stop);

    for (i = start; i < stop; i++)
    {
        a[i] = pfacSum(i);
    }
    //printf("The amicable pairs below 10,000 are:");
    for (n = 2; n < stop-1; n++) 
    {
        m = a[n];
        if (m >= n && m < stop-1 && n == a[m])
        {
            printf("(%d,%d) ", n, m);
        }
    }
}