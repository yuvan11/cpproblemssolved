/* 
 * Enter your code here. Read input from STDIN. Print your output to STDOUT. 
 * Your class should be named CandidateCode.
*/

import java.util.Scanner;
public class PowerPuff {
   public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
long test = scanner.nextInt();
long[] ingredients = new long[(int) test];
long[] laboratory = new long[(int) test];

for (int i = 0; i < test; i++) {
ingredients[i] = scanner.nextLong();
}

for (int i = 0; i < test; i++) {
laboratory[i] = scanner.nextLong();
}
scanner.close();
System.out.println(result(ingredients,laboratory, test));
}

private static long result(long[] ingredients, long[] laboratory, long size) {
long result = Long.MAX_VALUE;
System.out.println(result);
for (int i = 0; i < size; i++) {
if(laboratory[i]/ingredients[i] < result) result = laboratory[i]/ingredients[i];
}
return result;
}
}
