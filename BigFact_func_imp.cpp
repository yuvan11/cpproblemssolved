#include <iostream>
using namespace std;



#define MAX 100000

int multiplyNumbers(int x, int fin_array[], int res_size); 

void calculateFactorial(int num){
    
    
    int fin_array[MAX];
     int res_size = 1 ;
     fin_array[0] = 1 ;
    
    for(int itr = 2 ; itr <= num ; itr++) 
        res_size = multiplyNumbers( itr , fin_array , res_size );
    

    for(int itr = res_size-1 ; itr >= 0 ; itr--){
        cout << fin_array[itr] ;
    }
    
    cout << "\n";
    
    
    
}


int multiplyNumbers(int x , int fin_array[] , int res_size){
    
    int carry = 0 ;
    
    for(int i=0 ; i< res_size ; i++){
        int product = x * fin_array[i] + carry ;
        
        
        
        fin_array[i] = product % 10 ;
    
        carry = product / 10 ;


      }
      
      
      while(carry){
          
          
          fin_array[res_size ] = carry % 10 ;
          carry /= 10 ;
          res_size++ ;
      }
    return res_size ;
    
}

int main() {
    
    int t , N;
    cin >> t ;
 
   while(t--){ 
       
       cin >> N ;
       calculateFactorial(N);
      
       
       
   }
	return 0;
}
