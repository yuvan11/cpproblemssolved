#include <stdio.h>

int main()
{
    int N;
    scanf("%d", &N);
    int array[N];
    int loop, largest, second;

    for (int i = 0; i <= N; i++)
    {

        scanf("%d ", &array[i]);
    }

    if (array[0] > array[1])
    {
        largest = array[0];
        second = array[1];
    }
    else
    {
        largest = array[1];
        second = array[0];
    }

    for (loop = 2; loop < 10; loop++)
    {
        if (largest < array[loop])
        {
            second = largest;
            largest = array[loop];
        }
        else if (second < array[loop])
        {
            second = array[loop];
        }
    }

    printf("%d", second);

    return 0;
}
