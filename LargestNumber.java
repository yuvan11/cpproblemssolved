import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

class Solution {
    public String largestNumber(int[] nums) {

        String[] ss = new String[nums.length];
        for (int i = 0; i < nums.length; ++i) {
            ss[i] = String.valueOf(nums[i]);
        }
        Arrays.sort(ss, (a, b) -> (b + a).compareTo(a + b));
        if (ss[0].equals("0"))
            return "0";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < ss.length; i++) {
            sb.append(ss[i]);
        }
        return sb.toString();
    }
}

public class LargestNumber {
    public static int[] stringToIntegerArray(String input) {
        input = input.trim();
        input = input.substring(1, input.length() - 1);
        if (input.length() == 0) {
            return new int[0];
        }

        String[] parts = input.split(",");
        int[] output = new int[parts.length];
        for (int index = 0; index < parts.length; index++) {
            String part = parts[index].trim();
            output[index] = Integer.parseInt(part);
        }
        return output;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ((line = in.readLine()) != null) {
            int[] nums = stringToIntegerArray(line);

            String ret = new Solution().largestNumber(nums);

            String out = (ret);

            System.out.print(out);
        }
    }
}