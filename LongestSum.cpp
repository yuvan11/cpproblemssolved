#include <bits/stdc++.h> 
using namespace std; 
  
// Function to find the count of 
// longest subarrays with sum not 
// divisible by K 
int CountLongestSubarrays( 
    int arr[], int n, int k) 
{ 
  
    // Sum of all elements in 
    // an array 
    int i, s = 0; 
    for (i = 0; i < n; ++i) { 
        s += arr[i]; 
    } 
  
    // If overall sum is not 
    // divisible then return 
    // 1, as only one subarray 
    // of size n is possible 
    if (s % k) { 
        return 1; 
    } 
    else { 
        int ini = 0; 
  
        // Index of the first number 
        // not divisible by K 
        while (ini < n 
               && arr[ini] % k == 0) { 
            ++ini; 
        } 
  
        int final = n - 1; 
  
        // Index of the last number 
        // not divisible by K 
        while (final >= 0 
               && arr[final] % k == 0) { 
            --final; 
        } 
  cout << "Fin" << final << "\n" <<"Ind" << ini << "\n N val" << n ;


        int len, sum = 0, count = 0; 
        // Subarray doesn't exist 
        if (ini == n) { 
            return -1; 
        } 
        else { 
            len = max(n  - 1 - ini, 
                      final); 
	
    	} 
		cout << "\n Len" <<len <<endl;
  
        // Sum of the window 
        for (i = 0; i < len; i++) { 
            sum += arr[i]; 
        } 
	cout << "Sum " << sum << "\n";
  
        if (sum % k != 0) { 
            count++; 
        } 
cout << "Count" << count << "\n";
        // Calculate the sum of rest of 
        // the windows of size len 
        for (i = len; i < n; i++) { 
            sum = sum + arr[i]; 
            sum = sum - arr[i - len]; 
            if (sum % k != 0) { 
                count++; 
            } 
        } 
        return count; 
    } 
} 
  
// Driver Code 
int main() 
{ 
    int arr[] = { 3, 2, 2, 2, 3 }; 
    int n = sizeof(arr) 
            / sizeof(arr[0]); 
    int k = 3; 
    cout << CountLongestSubarrays(arr, n, k) << endl; 
    return 0; 
} 
